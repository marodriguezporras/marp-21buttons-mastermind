from restapi.games.models import Game, Move, CodePeg


def generate_game(code_to_break=None):
    game = Game()
    game.id = 1
    game.code_to_break = code_to_break
    game.save()
    return game


def generate_move(game=None):
    if not game:
        game = Game()
        game.save()

    move = Move()
    move.game_id = game.id
    move.formatted_code = [CodePeg.BLUE, CodePeg.BLUE, CodePeg.BLUE, CodePeg.BLUE]
    move.formatted_matches = []
    move.save()

    return move
