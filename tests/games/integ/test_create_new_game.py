import json

from flask import url_for
from restapi.games.models import Game
from tests.games.integ.base import BaseTestCase


class CreateGameTestCase(BaseTestCase):
    def test_whenPostNewGame_thenNewGameExists(self):
        self.assertEquals(len(Game.query.all()), 0)

        # WHEN
        response = self.api.post(
            url_for('games.create_new_game'),
            headers=self.get_request_headers(),
            data=json.dumps({}))

        # THEN
        self.assertEqual(response.status_code, 201)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertIn('id', json_response['data'])
        self.assertEquals(len(Game.query.all()), 1)
