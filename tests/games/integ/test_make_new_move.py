import datetime
import json

from flask import url_for
from restapi.games.models import Game, Move
from tests.games.integ import generate_game
from tests.games.integ.base import BaseTestCase


class MakeMoveTestCase(BaseTestCase):
    def test_givenGame_whenCodeWithPartialMatch_thenBWW(self):
        # GIVEN
        self.assertEquals(len(Move.query.all()), 0)
        game = generate_game(code_to_break="RED,BLUE,GREEN,RED")

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers(),
            data=json.dumps({"code": ["RED", "GREEN", "RED", "YELLOW"]}))

        # THEN
        self.assertEqual(response.status_code, 201)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEqual(json_response['data']['matches'], ['BLACK','WHITE','WHITE'])
        self.assertEquals(len(Move.query.all()), 1)

    def test_givenGameWithSameColors_whenCodeWithTwoMatches_thenBB(self):
        # GIVEN
        self.assertEquals(len(Move.query.all()), 0)
        game = generate_game(code_to_break="RED,RED,RED,RED")

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers(),
            data=json.dumps({"code": ["RED", "GREEN", "RED", "YELLOW"]}))

        # THEN
        self.assertEqual(response.status_code, 201)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEqual(json_response['data']['matches'], ['BLACK', 'BLACK'])
        self.assertEquals(len(Move.query.all()), 1)

    def test_givenGame_whenCodeWithTotalMatch_thenGameIsWon(self):
        # GIVEN
        self.assertEquals(len(Move.query.all()), 0)
        game = generate_game(code_to_break="RED,BLUE,GREEN,RED")

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers(),
            data=json.dumps({"code": ["RED", "BLUE", "GREEN", "RED"]}))

        # THEN
        self.assertEqual(response.status_code, 201)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEqual(json_response['data']['matches'], ['BLACK','BLACK','BLACK','BLACK'])
        game = Game.get_by(id=game.id)
        self.assertTrue(game.is_won)
        self.assertEquals(len(Move.query.all()), 1)

    def test_givenGame_whenNotEnoughPegs_thenFail(self):
        # GIVEN
        self.assertEquals(len(Move.query.all()), 0)
        game = generate_game()

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers(),
            data=json.dumps({"code": ["RED", "BLUE", "GREEN"]}))

        # THEN
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(json_response['response']['message'], "Exactly 4 pegs should be provided")
        self.assertEquals(len(Move.query.all()), 0)

    def test_givenGame_whenNotPegs_thenFail(self):
        # GIVEN
        self.assertEquals(len(Move.query.all()), 0)
        game = generate_game()

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers())

        # THEN
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(json_response['response']['message'], "Invalid request code must be supplied")
        self.assertEquals(len(Move.query.all()), 0)

    def test_givenGame_whenTooManyPegs_thenFail(self):
        # GIVEN
        game = generate_game()
        self.assertEquals(len(Move.query.all()), 0)

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers(),
            data = json.dumps({"code": ["RED", "BLUE", "GREEN", "BLUE", "GREEN"]}))

        # THEN
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(json_response['response']['message'], "Exactly 4 pegs should be provided")
        self.assertEquals(len(Move.query.all()), 0)

    def test_whenInvalidGame_thenFail(self):
        self.assertEquals(len(Move.query.all()), 0)

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=1),
            headers=self.get_request_headers(),
            data=json.dumps({"code": ["BLUE", "BLUE", "RED", "YELLOW"]}))

        # THEN
        self.assertEqual(response.status_code, 404)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(json_response['response']['message'], "Game instance not found")
        self.assertEquals(len(Move.query.all()), 0)

    def test_givenCompletedGame_whenMakeMove_thenFail(self):
        # GIVEN
        game = generate_game()
        game.completed_date = datetime.datetime.utcnow()
        game.save()
        self.assertEquals(len(Move.query.all()), 0)

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers(),
            data=json.dumps({"code": ["RED", "GREEN", "RED", "YELLOW"]}))

        # THEN
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(json_response['response']['message'], "Game is already completed")
        self.assertEquals(len(Move.query.all()), 0)

    def test_givenGame_whenInvalidPegs_thenFail(self):
        # GIVEN
        game = generate_game()
        self.assertEquals(len(Move.query.all()), 0)

        # WHEN
        response = self.api.post(
            url_for('games.make_new_move', game_id=game.id),
            headers=self.get_request_headers(),
            data=json.dumps({"code": ["RED", "GREEN", "RED", "GRAY"]}))

        # THEN
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(json_response['response']['message'], "Invalid peg color: GRAY")
        self.assertEquals(len(Move.query.all()), 0)
