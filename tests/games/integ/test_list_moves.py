import json

from flask import url_for
from restapi.games.models import Move
from tests.games.integ import generate_game, generate_move
from tests.games.integ.base import BaseTestCase


class ListMovesTestCase(BaseTestCase):
    def test_givenMoveList_whenMoveListRequested_thenAllMoveReturned(self):
        # GIVEN
        self.assertEquals(len(Move.query.all()), 0)
        game = generate_game()
        for i in range(5):
            move = generate_move(game=game)
        self.assertEquals(len(Move.query.all()), 5)

        # WHEN
        response = self.api.get(
            url_for('games.list_moves', game_id=game.id),
            headers=self.get_request_headers())

        # THEN
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(len(json_response['data']), len(Move.query.all()))
        self.assertEquals(json_response['data'][4]['id'], move.id)

    def test_whenNoMoves_thenEmtpyResponse(self):
        game = generate_game()
        self.assertEquals(len(Move.query.all()), 0)

        response = self.api.get(
            url_for('games.list_moves', game_id=game.id),
            headers=self.get_request_headers())

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(len(json_response['data']), len(Move.query.all()))

    def test_whenNoGame_thenFail(self):
        # WHEN
        response = self.api.get(
            url_for('games.list_moves', game_id=1),
            headers=self.get_request_headers())

        # THEN
        self.assertEqual(response.status_code, 404)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertEquals(json_response['response']['message'], "Game instance not found")
