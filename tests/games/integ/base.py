import unittest
from restapi import create_app, db


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.api = self.app.test_client()
        db.create_all()

    def tearDown(self):
        db.drop_all()
        self.app_context.pop()

    def get_request_headers(self):
        return {
            'Content-Type': 'application/json',
        }
