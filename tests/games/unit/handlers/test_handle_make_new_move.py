import unittest
from mock import patch, Mock
from restapi.extra.factory import RsError
from restapi.games.endpoints.handlers import handle_make_new_move


class HandlerMakeNewMoveTestCase(unittest.TestCase):
    @patch('restapi.games.endpoints.handlers.move.create_new_move')
    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_givenRequest_whenHandlerMakeNewMove_thenCreateMoveIsCalled(self, game_model_mock, create_new_move_mock):
        # GIVEN
        game_id = 1
        request_mock = Mock(data=True, is_json=True, json={'code':["RED", "GREEN", "RED", "YELLOW"]})
        game_mock = Mock(**{'is_completed.return_value': False})
        game_model_mock.get_by.return_value = game_mock

        # WHEN
        handle_make_new_move(request_mock, game_id)

        # THEN
        create_new_move_mock.assert_called_with(game_mock, ["RED", "GREEN", "RED", "YELLOW"])

    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_givenInvalidGame_whenHandlerMakeNewMove_thenThrowError(self, game_model_mock):
        # GIVEN
        game_id = 1
        game_model_mock.get_by.return_value = None

        # WHEN
        with self.assertRaises(RsError) as context:
            handle_make_new_move(None, game_id)

        # THEN
        self.assertEquals('Game instance not found', context.exception.ms)
        self.assertEquals(404, context.exception.status)

    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_givenCompletedGame_whenHandlerMakeNewMove_thenThrowError(self, game_model_mock):
        # GIVEN
        game_id = 1
        game_mock = Mock(**{'is_completed.return_value': True})
        game_model_mock.get_by.return_value = game_mock

        # WHEN
        with self.assertRaises(RsError) as context:
            handle_make_new_move(None, game_id)

        # THEN
        self.assertEquals('Game is already completed', context.exception.ms)
        self.assertEquals(400, context.exception.status)

    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_givenInvalidRequest_whenHandlerMakeNewMove_thenThrowError(self, game_model_mock):
        # GIVEN
        game_id = 1
        request_mock = Mock(data=True, is_json=True, json={'c': ["RED", "GREEN", "RED", "YELLOW"]})
        game_mock = Mock(**{'is_completed.return_value': False})
        game_model_mock.get_by.return_value = game_mock

        # WHEN
        with self.assertRaises(RsError) as context:
            handle_make_new_move(request_mock, game_id)

        # THEN
        self.assertEquals('Invalid request code must be supplied', context.exception.ms)
        self.assertEquals(400, context.exception.status)

    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_givenInvalidCodeLength_whenHandlerMakeNewMove_thenThrowError(self, game_model_mock):
        # GIVEN
        game_id = 1
        request_mock = Mock(data=True, is_json=True, json={'code': ["RED", "GREEN", "RED"]})
        game_mock = Mock(**{'is_completed.return_value': False})
        game_model_mock.get_by.return_value = game_mock

        # WHEN
        with self.assertRaises(RsError) as context:
            handle_make_new_move(request_mock, game_id)

        # THEN
        self.assertEquals('Exactly 4 pegs should be provided', context.exception.ms)
        self.assertEquals(400, context.exception.status)

    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_givenInvalidPegColor_whenHandlerMakeNewMove_thenThrowError(self, game_model_mock):
        # GIVEN
        game_id = 1
        request_mock = Mock(data=True, is_json=True, json={'code': ["RED", "GREEN", "RED", "GREY"]})
        game_mock = Mock(**{'is_completed.return_value': False})
        game_model_mock.get_by.return_value = game_mock

        # WHEN
        with self.assertRaises(RsError) as context:
            handle_make_new_move(request_mock, game_id)

        # THEN
        self.assertEquals('Invalid peg color: GREY', context.exception.ms)
        self.assertEquals(400, context.exception.status)
