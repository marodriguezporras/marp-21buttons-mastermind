import unittest
from mock import patch
from restapi.extra.factory import RsError
from restapi.games.endpoints.handlers import handle_list_moves


class HandlerListMovesTestCase(unittest.TestCase):
    @patch('restapi.games.endpoints.handlers.move.list_moves')
    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_whenHandlerNewGameRequest_thenCreateGameIsCalled(self, game_model_mock, list_moves_mock):
        game_id = 1

        # WHEN
        handle_list_moves(game_id)

        # THEN
        list_moves_mock.assert_called()

    @patch('restapi.games.endpoints.handlers.move.Game')
    def test_givenInvalidGame_whenHandlerNewGameRequest_thenThrowError(self, game_model_mock):
        # GIVEN
        game_id = 1
        game_model_mock.get_by.return_value = None

        # WHEN
        with self.assertRaises(RsError) as context:
            handle_list_moves(game_id)

        # THEN
        self.assertEquals('Game instance not found', context.exception.ms)
        self.assertEquals(404, context.exception.status)


