import unittest
from mock import patch
from restapi.games.endpoints.handlers import handle_create_new_game


class HandlerCreateGameTestCase(unittest.TestCase):
    @patch('restapi.games.endpoints.handlers.game.create_new_game')
    def test_whenHandlerNewGameRequest_thenCreateGameIsCalled(self, create_new_game_mock):
        # WHEN
        handle_create_new_game()

        # THEN
        create_new_game_mock.assert_called()


