import unittest
from mock import patch, Mock
from restapi.games.business.move import list_moves


class ListMovesTestCase(unittest.TestCase):
    @patch('restapi.games.business.move.Move')
    def test_givenGame_whenListMoves_thenMovesAreQueried(self, move_filter):
        # GIVEN
        game = Mock(id=1)
        move_filter.query.filter_by.return_value = []

        # WHEN
        list_moves(game)

        # THEN
        move_filter.query.filter_by.assert_called_with(game_id=1)
