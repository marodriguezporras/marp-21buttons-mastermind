import unittest
from collections import defaultdict
from mock import patch, Mock
from restapi.games.business.move import create_new_move
import datetime


class MakeMoveTestCase(unittest.TestCase):
    @patch('restapi.games.business.move.Move')
    def test_givenGame_whenCreateNewMove_thenMoveIsSaved(self, move_model_mock):
        # GIVEN
        game = Mock(id=1,
                    moves=[1],
                    dict_code_to_break=defaultdict(list, {'RED': [0, 3], 'BLUE': [1], 'GREEN': [2]}))
        move_model_mock.return_value.is_winning_move.return_value = False

        # WHEN
        move = create_new_move(game, ["RED", "GREEN", "RED", "YELLOW"])

        # THEN
        self.assertEquals(move.game_id, game.id)
        self.assertEquals(move.formatted_code, ['RED', 'GREEN', 'RED', 'YELLOW'])
        self.assertEquals(move.formatted_matches, ['BLACK', 'WHITE', 'WHITE'])
        move_model_mock.return_value.save.assert_called()

    @patch('restapi.games.business.move.Move')
    def test_givenGame_whenCreateWinningMove_thenGameIsWon(self, move_model_mock):
        # GIVEN
        game = Mock(id=1,
                    moves=[1],
                    dict_code_to_break=defaultdict(list, {'RED': [0, 3], 'BLUE': [1], 'GREEN': [2]}))
        move_model_mock.return_value.is_winning_move.return_value = True

        # WHEN
        move = create_new_move(game, ["RED", "BLUE", "GREEN", "RED"])

        # THEN
        self.assertEquals(move.game_id, game.id)
        self.assertEquals(move.formatted_code, ["RED", "BLUE", "GREEN", "RED"])
        self.assertEquals(move.formatted_matches, ['BLACK', 'BLACK', 'BLACK', 'BLACK'])
        move_model_mock.return_value.save.assert_called()
        self.assertEquals(game.completed_date.day, datetime.datetime.utcnow().day)
        self.assertTrue(game.is_won)
        game.save.assert_called()

    @patch('restapi.games.business.move.Move')
    def test_givenGame_whenCreateLastMove_thenGameIsCompleted(self, move_model_mock):
        # GIVEN
        game = Mock(id=1,
                    moves=range(12),
                    max_turns=12,
                    dict_code_to_break=defaultdict(list, {'RED': [0, 3], 'BLUE': [1], 'GREEN': [2]}))
        move_model_mock.return_value.is_winning_move.return_value = False

        # WHEN
        move = create_new_move(game, ["RED", "GREEN", "RED", "YELLOW"])

        # THEN
        self.assertEquals(move.game_id, game.id)
        self.assertEquals(move.formatted_code, ["RED", "GREEN", "RED", "YELLOW"])
        self.assertEquals(move.formatted_matches, ['BLACK', 'WHITE', 'WHITE'])
        move_model_mock.return_value.save.assert_called()
        self.assertEquals(game.completed_date.day, datetime.datetime.utcnow().day)
        game.is_won.assert_not_called()
        game.save.assert_called()
