import unittest
from mock import patch
from restapi.games.business.game import create_new_game


class CreateGameTestCase(unittest.TestCase):
    @patch('restapi.games.business.game.Game')
    def test_whenCreateNewGame_thenGameIsSaved(self, game_model_mock):
        # WHEN
        create_new_game()

        # THEN
        game_model_mock.return_value.save.assert_called()


