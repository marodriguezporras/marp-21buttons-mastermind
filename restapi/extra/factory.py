from flask import Response, json


class RsError(Exception):
    """
    Factory used to macro a base response error schema for this application.
    """
    def __init__(self, status=400, ms=None):
        self.status = status
        self.ms = ms
        super(RsError, self).__init__(self.ms)

    def send(self):
        r = {
            'response': {
                'status': int(self.status),
                'message': self.ms,
            }
        }
        return Response(json.dumps(r),
                        status=self.status,
                        mimetype='application/json')


class Rs:
    """
    Factory used to macro a base response schema for this application.
    """
    def __init__(self, status=200, data=None, ms=None):
        self.data = data
        self.status = status
        self.ms = ms

    def send(self):

        r = {
            'data': self.data,
            'response': {
                'status':               int(self.status),
                'message':              self.ms,
            }
        }

        return Response(json.dumps(r),
                        status=self.status,
                        mimetype='application/json')

