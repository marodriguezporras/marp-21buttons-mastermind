from flask import Blueprint
from restapi.extra.factory import Rs
from restapi import db


resource = Blueprint('deploy', __name__)


@resource.route('/deploy/db', methods=['POST'])
def deploy_db():
    db.create_all()
    return Rs().send()
