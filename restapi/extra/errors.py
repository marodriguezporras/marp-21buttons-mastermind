from flask import Blueprint
from restapi.extra.factory import RsError

err = Blueprint('errors', __name__)


@err.app_errorhandler(RsError)  # Error Handler for custom app raised errors
def custom_app_error(rse):
    return rse.send()


@err.app_errorhandler(404)
def not_found(e):
    rse = RsError()
    rse.status = 404
    rse.ms = 'The resource referenced in the URL was not found.'
    return rse.send()
