from restapi import db
from datetime import datetime
from .peg import KeyPeg
from sqlalchemy.util.compat import reduce


class Move(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    code = db.Column(db.String(100), nullable=False)
    matches = db.Column(db.String(100), nullable=False)
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), nullable=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._formatted_code = None
        self._formatted_matches = None

    def __repr__(self):
        return '<Move %r>' % self.id

    @property
    def formatted_code(self):
        return self.code.split(',')

    @formatted_code.setter
    def formatted_code(self, formatted_code):
        self.code = ','.join(formatted_code)

    @property
    def formatted_matches(self):
        return self.matches.split(',')

    @formatted_matches.setter
    def formatted_matches(self, formatted_matches):
        self.matches = ','.join(formatted_matches)

    def is_winning_move(self):
        return len(self.formatted_code) == \
               reduce(lambda x, y: x + y,
                      map(lambda x: 1 if x == KeyPeg.BLACK else 0, self.formatted_matches))

    def to_dict(self):
        return {'id': self.id,
                'created_date': self.created_date.strftime("%Y-%m-%d %H:%M"),
                'code': self.formatted_code,
                'matches': self.formatted_matches
                }

    def save(self):
        db.session.add(self)
        db.session.commit()
