from collections import defaultdict
from datetime import datetime
from restapi import db
from .peg import CodePeg
import random


class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code_to_break = db.Column(db.String(100), nullable=False)
    max_turns = db.Column(db.Integer, nullable=False, default=12)
    created_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    completed_date = db.Column(db.DateTime)
    is_won = db.Column(db.Boolean, default=False)
    moves = db.relationship('Move', backref='game', lazy=True)

    def __repr__(self):
        return '<Game %r>' % self.id

    def generate_code(self):
        pegs = []
        for i in range(4):
            pegs.append(CodePeg.ALL[random.randint(0, len(CodePeg.ALL)-1)])
        self.code_to_break = ','.join(pegs)

    def to_dict(self):
        return {'id': self.id, 'max_turns': self.max_turns}

    @staticmethod
    def get_by(**q_param):
        return Game.query.filter_by(**q_param).first()

    def is_completed(self):
        return self.completed_date is not None

    @property
    def dict_code_to_break(self):
        if not hasattr(self, '_dict_code_to_break'):
            if not self.code_to_break:
                self.generate_code()
            code_list = self.code_to_break.split(',')
            self._dict_code_to_break = defaultdict(list)
            for i, code in enumerate(code_list):
                self._dict_code_to_break[code].append(i)

        return self._dict_code_to_break

    def save(self):
        if not self.code_to_break:
            self.generate_code()
        db.session.add(self)
        db.session.commit()



