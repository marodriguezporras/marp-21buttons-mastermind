class CodePeg:
    RED = 'RED'
    BLUE = 'BLUE'
    GREEN = 'GREEN'
    YELLOW = 'YELLOW'
    PURPLE = 'PURPLE'
    BROWN = 'BROWN'
    ALL = [RED, BLUE, GREEN, YELLOW, PURPLE, BROWN]


class KeyPeg:
    BLACK = 'BLACK'
    WHITE = 'WHITE'
