from restapi.games.models import KeyPeg, Move
import datetime


def create_new_move(game, code_breaker):
    code_to_break = game.dict_code_to_break
    color_matches = []
    index_matches = set()
    for i, color in enumerate(code_breaker):
        if i in code_to_break[color]:
            color_matches.append(KeyPeg.BLACK)
            index_matches.add(i)
            code_to_break[color].remove(i)

    for i, color in enumerate(code_breaker):
        if i not in index_matches and code_to_break[color]:
            color_matches.append(KeyPeg.WHITE)
            code_to_break[color].pop(0)

    move = Move()
    move.game_id = game.id
    move.formatted_code = code_breaker
    move.formatted_matches = color_matches
    move.save()

    if move.is_winning_move():
        game.completed_date = datetime.datetime.utcnow()
        game.is_won = True
        game.save()
    elif len(game.moves) == game.max_turns:
        game.completed_date = datetime.datetime.utcnow()
        game.save()

    return move


def list_moves(game):
    moves = Move.query.filter_by(game_id=game.id)
    return moves
