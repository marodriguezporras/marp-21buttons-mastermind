from flask import Blueprint, request
from restapi.extra.factory import Rs
from restapi.games.endpoints.handlers import handle_create_new_game, handle_make_new_move, handle_list_moves

resource = Blueprint('games', __name__)


@resource.route('/games', methods=['POST'])
def create_new_game():
    """
    Creates a new game.
    ---
    definitions:
      Game:
         type: object
         properties:
           id:
             type: integer
           max_turns:
             type: integer
    responses:
      201:
        description: The new game created
        examples:
           game: {id: 1, max_turns: 12}

    """
    game = handle_create_new_game()
    return Rs(status=201, data=game.to_dict()).send()


@resource.route('/games/<int:game_id>/moves', methods=['POST'])
def make_new_move(game_id):
    """
    Creates a new move that tries to solve the provided game id
    ---
    parameters:
      - name: code
        in: body
        type: array
        items:
          type: string
          enum: ['RED', 'BLUE', 'GREEN', 'YELLOW', 'PURPLE', 'BROWN']
        example: ['RED', 'BLUE', 'GREEN', 'YELLOW']
        required: true
    responses:
      201:
        description: A move detail including the move matches. All 4 BLACK matches means the game is won
        examples:
          move: {
            id: 1,
            code: ['RED', 'BLUE', 'GREEN', 'YELLOW'],
            matches: ['BLACK', 'WHITE'],
            created_date: '2019-12-03 10:24'
          }
      404:
         description: Game id does not exists
      400:
         description: Invalid request
    """
    move = handle_make_new_move(request, game_id)
    return Rs(status=201, data=move.to_dict()).send()


@resource.route('/games/<int:game_id>/moves', methods=['GET'])
def list_moves(game_id):
    """
    Returns the list of (historic) moves that belongs to the game id provided
    ---
    parameters:
      - name: game id
        in: path
        type: integer
        required: true
        description: id of the game requesting the moves from
    definitions:
      Move:
        type: object
        properties:
          id:
            type: integer
          created_date:
            type: date
          code:
            type: array
            items:
              type: string
              enum: ['RED', 'BLUE', 'GREEN', 'YELLOW', 'PURPLE', 'BROWN']
          matches:
            type: array
            items:
              type: string
              enum: ['BLACK', 'WHITE']
    responses:
      200:
        description: A list of moves from the given game id
        schema:
          $ref: '#/definitions/Move'
        examples:
           moves: [{
             id: 1,
             code: ['RED', 'BLUE', 'GREEN', 'YELLOW'],
             matches: ['BLACK', 'WHITE'],
             created_date: '2019-12-03 10:24'
           }]
      404:
         description: Game id does not exists
    """
    moves = handle_list_moves(game_id)
    return Rs(status=200, data=list(map(lambda x: x.to_dict(), moves))).send()
