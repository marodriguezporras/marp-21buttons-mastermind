from restapi.extra.factory import RsError
from restapi.games.business.move import create_new_move, list_moves
from restapi.games.models import Game, CodePeg


def handle_make_new_move(request, game_id):
    game = Game.get_by(id=game_id)
    if not game:
        raise RsError(status=404, ms="Game instance not found")

    if game.is_completed():
        raise RsError(status=400, ms="Game is already completed")

    if not request.data or not request.is_json or 'code' not in request.json:
        raise RsError(status=400, ms="Invalid request code must be supplied")

    code_breaker = request.json['code']
    if len(code_breaker) != 4:
        raise RsError(status=400, ms="Exactly 4 pegs should be provided")

    for peg in code_breaker:
        if peg not in CodePeg.ALL:
            raise RsError(status=400, ms="Invalid peg color: %s" % peg)

    return create_new_move(game, code_breaker)


def handle_list_moves(game_id):
    game = Game.get_by(id=game_id)
    if not game:
        raise RsError(status=404, ms="Game instance not found")

    return list_moves(game)
