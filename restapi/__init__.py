from config.config_dev import Development
from config.config_test import Testing
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flasgger import Swagger

# A list of context configurations available
SETTINGS = {
    'development': Development,
    'testing': Testing
}

db = SQLAlchemy()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(SETTINGS[config_name])
    db.init_app(app)
    # Create an APISpec
    template = dict(
        info={
            'title': 'm.a.r.p Mastermind API',
            'version': '0.0.1',
            'description': '21Buttons BE Test',
        }
    )
    swagger = Swagger(app, template=template)
    register_local_modules(app)
    return app


def register_local_modules(app):
    from restapi.extra.errors import err
    app.register_blueprint(err)

    from restapi.extra.deploy import resource as deploy_resource
    app.register_blueprint(deploy_resource)

    from restapi.games.endpoints.endpoints import resource as game_resource
    app.register_blueprint(game_resource)
