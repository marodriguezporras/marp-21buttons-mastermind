class Config:
    """
    Base configuration class
    """

    def __init__(self):
        pass

    # Application
    APP_NAME = 'mastermind-21'
    APP_URL = 'https://www.mastermind.21buttons.com'
    DEBUG = True

    # Werkezeug, set development defaults
    LISTEN_HOST = '0.0.0.0'
    WERKZEUG_OPTS = {'host': LISTEN_HOST, 'port': 5000}

    # Database
    SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
