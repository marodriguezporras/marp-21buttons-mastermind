from config.config import Config


class Development(Config):
    """
    Development Configuration Settings
    """

    ENV = 'development'
