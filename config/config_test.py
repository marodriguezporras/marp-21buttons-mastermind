from config.config import Config


class Testing(Config):
    """
    Testing Configuration Settings
    """
    ENV = 'testing'
    TESTING = True

    # To get url_for working properly
    SERVER_NAME = 'localhost:5000'

    # Database
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test-db.sqlite'
