# m.a.r.p Mastermind API

21 Buttons Backend Coding Challenge.

Rest API that simulates the role of the Masterminds codemaker.

This API has a single module (games) where most of the code resides.

Within the games module, the code is divided in:

 * Business Logic (business folder)
 * Endpoints and request handlers (endpoints folder)
 * Database models definition (models folder)

## Usage and features

The API can:

 * Create game
 * Return feedback given a code guess
 * Check game historic moves

## Important notes

 * The challenge was developed and built with Python3 and Flask
 * To get the API up and running, only the runserver.py script must be 
  executed ('$ python runserver.py')
 * Once the runserver.py is script is running, the API can be found at
  http://0.0.0.0:5000/
 * Before consuming the API, the database must be initialized. With the 
  server running, do a POST /deploy/db to initialize the database. This
  endpoint is intended as an easy mechanism for you to start using the API,
  is not something that would be left open in a production environment.
 * Once the runserver.py is script is running, the docs can be found at
  http://0.0.0.0:5000/apidocs
 
## Tools and libraries

The project uses the following external libraries (see the requirements.txt
for details):

 * **flask**: As web application framework
 * **SQLAlchemy**: As the API's ORM
 * **mock**: For testing and mocking
 * **flasgger**: For Swagger docs generation  
  
The justification behind the use of Flask is that this is lightweight 
extensible framework, which makes it a perfect fit for getting started 
small projects such as this one in a very quick and easy manner. 
There are many extensions available to Flask, so Flask projects can also
grow to become complex applications if needed.

## Testing

Both unit and integration tests were writen. Specifically the business and 
handler layers have their corresponding unit tests, whereas the integration
tests test the API as a whole.