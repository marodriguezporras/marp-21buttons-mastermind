from restapi import create_app

FLASK_CONFIG = 'development'

app = create_app(FLASK_CONFIG)

if __name__ == '__main__':
    app.run(**app.config['WERKZEUG_OPTS'])
